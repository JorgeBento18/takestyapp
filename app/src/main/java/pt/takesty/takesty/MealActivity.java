package pt.takesty.takesty;


import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.etiennelawlor.imagegallery.library.activities.ImageGalleryActivity;
import com.etiennelawlor.imagegallery.library.enums.PaletteColorType;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import Models.Meal;

public class MealActivity extends Activity implements View.OnClickListener {

    private ImageView mealPhoto_imageview;
    private TextView mealTitle_textview;
    private TextView mealPrice_textview;
    private TextView mealDescription_textview;
    private ImageView meal_user_photo_imageview;
    private TextView meal_user_name_textview;
    private TextView meal_user_description_textview;
    private Meal meal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meal);

        mealPhoto_imageview = (ImageView) findViewById(R.id.meal_photo_imageview);

        mealPhoto_imageview.setOnClickListener(this);

        mealTitle_textview = (TextView) findViewById(R.id.meal_title_textview);
        mealPrice_textview = (TextView) findViewById(R.id.meal_price_textview);
        mealDescription_textview = (TextView) findViewById(R.id.meal_description_textview);

        meal_user_photo_imageview = (ImageView) findViewById(R.id.meal_username_photo);

        meal_user_name_textview = (TextView) findViewById(R.id.meal_username_textview);

        meal_user_description_textview = (TextView) findViewById(R.id.meal_username_description_textview);

        meal = (Meal) getIntent().getSerializableExtra("meal");

        setTitle(meal.getTitle());

        if (meal.getImages() == null) {

            Picasso.with(getApplicationContext()).load("http://192.168.119.129/takesty/public/assets/img/meals/no_meal_image.png").into(mealPhoto_imageview);
        } else {

            if (meal.getImages().size() == 0) {

                Picasso.with(getApplicationContext()).load("http://192.168.119.129/takesty/public/assets/img/meals/no_meal_image.png").into(mealPhoto_imageview);

            } else {
                Picasso.with(getApplicationContext()).load("http://192.168.119.129/takesty/public/assets/img/meals/" + meal.getImages().get(0).getPath()).into(mealPhoto_imageview);
            }
        }

        mealTitle_textview.setText(meal.getTitle());
        mealPrice_textview.setText(meal.getPrice() + "€");
        mealDescription_textview.setText(meal.getDescription());

        Picasso.with(getApplicationContext()).load("http://192.168.119.129/takesty/public/assets/img/account/"+meal.getUser().getPhoto()).into(meal_user_photo_imageview);

        meal_user_name_textview.setText(meal.getUser().getFirstName()+" " +meal.getUser().getLastName());
        meal_user_description_textview.setText("Descrição do utilizador");

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.meal_photo_imageview:

                if (meal.getImages() == null) {

                    Toast.makeText(getApplicationContext(), "Não existem fotografias disponíveis.", Toast.LENGTH_SHORT).show();

                } else {

                    if (meal.getImages().size() == 0) {

                        Toast.makeText(getApplicationContext(), "Não existem fotografias disponíveis.", Toast.LENGTH_SHORT).show();

                    } else {

                        Intent intent = new Intent(MealActivity.this, ImageGalleryActivity.class);

                        ArrayList<String> images = new ArrayList<>();

                        for (int i = 0; i < meal.getImages().size(); i++) {

                            images.add("http://192.168.119.129/takesty/public/assets/img/meals/" + meal.getImages().get(i).getPath());

                            System.out.println("Imagem "+ i + "http://192.168.119.129/takesty/public/assets/img/meals/" + meal.getImages().get(i).getPath());
                        }

                        intent.putStringArrayListExtra("images", images);

                        intent.putExtra("palette_color_type", PaletteColorType.LIGHT_VIBRANT);

                        startActivity(intent);
                    }
                }


                break;

            default:
                break;


        }
    }
}
