package Adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.LinkedList;

import Models.Meal;

import pt.takesty.takesty.R;
import pt.takesty.takesty.MealActivity;

/**
 * Created by Jorge on 31-08-2015.
 */
public class SearchResultAdapter extends BaseAdapter {

    private LinkedList<Meal> entries;
    private Context context;

    public SearchResultAdapter(LinkedList<Meal> entries, Context context) {
        this.entries = entries;
        this.context = context;
    }

    @Override
    public int getCount() {

        return this.entries.size();
    }

    @Override
    public long getItemId(int position) {

        return entries.get(position).getId();
    }

    @Override
    public Meal getItem(int position) {

        return this.entries.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {

            // Inflate the layout according to the view type
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(R.layout.search_result,
                    parent, false);
        }

        final Meal m = entries.get(position);

        TextView title = (TextView) convertView
                .findViewById(R.id.title_textview);

        title.setText(m.getTitle());

        TextView price = (TextView) convertView
                .findViewById(R.id.price_textview);

        price.setText(m.getPrice() + "€");

        TextView username = (TextView) convertView
                .findViewById(R.id.username_textview);

        username.setText(m.getUser().getFirstName() + " " + m.getUser().getLastName());

        ImageView mealImage = (ImageView) convertView.findViewById(R.id.meal_photo_imageview);

        if (m.getImages() == null) {

            Picasso.with(context).load("http://192.168.119.129/takesty/public/assets/img/meals/no_meal_image.png").into(mealImage);
        } else {

            if (m.getImages().size() == 0) {

                Picasso.with(context).load("http://192.168.119.129/takesty/public/assets/img/meals/no_meal_image.png").into(mealImage);

            } else {
                Picasso.with(context).load("http://192.168.119.129/takesty/public/assets/img/meals/" + m.getImages().get(0).getPath()).into(mealImage);
            }
        }

        //  Picasso.with(context).load("http://www.takesty.com/assets/img/meals/meal_43_IMG_20150706_214112_1436709394.jpg").into(mealImage);

        ImageView userImage = (ImageView) convertView.findViewById(R.id.user_photo_imageview);

        Picasso.with(context).load("http://192.168.119.129/takesty/public/assets/img/account/" + m.getUser().getPhoto()).into(userImage);

        //  Picasso.with(context).load("http://www.takesty.com/assets/img/account/Jorge_Bento_8_1430935961.jpg").into(userImage);

        convertView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                Intent intent = new Intent(context, MealActivity.class);

                intent.putExtra("meal", m);

                context.startActivity(intent);

            }
        });

        return convertView;
    }
}
