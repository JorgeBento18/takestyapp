package Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import pt.takesty.takesty.PreSearchActivity;

import pt.takesty.takesty.R;


public class HomeFragment extends Fragment implements View.OnClickListener {

    EditText searchEditText;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_home, container, false);

        searchEditText = (EditText) view.findViewById(R.id.searchEditText);

        searchEditText.setOnClickListener(this);

        return view;
    }


    @Override
    public void onClick(View v) {


        switch (v.getId()) {

            case R.id.searchEditText:

                Intent intent = new Intent(getActivity(), PreSearchActivity.class);

                getActivity().startActivity(intent);

                break;

            default:

                break;

        }
    }

}
