package Parsers;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import Models.Meal;
import Models.MealImage;
import Models.MealLocation;
import Models.MealSchedule;
import Models.User;

/**
 * Created by Jorge on 31-08-2015.
 */
public class SearchResultJSONParser {

    /**
     * Receives a JSONObject and returns a list
     */
    public LinkedList<Meal> parse(JSONObject jObject) {

        JSONArray jResults = null;
        try {
            /** Retrieves all the elements in the 'places' array */
            jResults = jObject.getJSONArray("meals");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        /**
         * Invoking getResults with the array of json object where each json
         * object represent a meal
         */
        return getResults(jResults);
    }

    private LinkedList<Meal> getResults(JSONArray jMeals) {

        int resultsCount = jMeals.length();

        LinkedList<Meal> mealList = new LinkedList<Meal>();

        Meal meal = new Meal();

        /** Taking each meal, parses and adds to list object */
        for (int i = 0; i < resultsCount; i++) {
            try {
                /** Call getPlace with place JSON object to parse the place */
                meal = getMeal((JSONObject) jMeals.get(i));
                mealList.add(meal);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return mealList;
    }

    /**
     * Parsing the Meal JSON object
     */
    private Meal getMeal(JSONObject jMeal) {

        int id = 0;
        String title = "";
        String description = "";
        String contact = "";
        double price = 0;
        String category = "";
        int aproved = 0;
        int shared = 0;

        MealLocation location = new MealLocation();

        User user = new User();

        LinkedList<MealSchedule> schedules = new LinkedList<MealSchedule>();

        LinkedList<MealImage> images = new LinkedList<MealImage>();

        try {

            id = jMeal.getInt("id");
            title = jMeal.getString("title");
            description = jMeal.getString("description");
            contact = jMeal.getString("contact");
            price = jMeal.getDouble("price");
            category = jMeal.getString("category");
            aproved = jMeal.getInt("approved");
            shared = jMeal.getInt("shared");

            location = getMealLocation(jMeal.getJSONObject("meal_location"));

            user = getUser(jMeal.getJSONObject("user"));

            schedules = getSchedules(jMeal.getJSONArray("meal_schedule"));

            images = getImages(jMeal.getJSONArray("meal_image"));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Meal meal = new Meal(id, title, description, contact, price, category, aproved, shared, user, location, schedules, images);

        return meal;
    }

    /**
     * Parsing the MealLocation JSON object
     */
    private MealLocation getMealLocation(JSONObject jMeal) {

        int id = 0;
        String address = "";
        String latitude = "";
        String longitude = "";

        try {

            id = jMeal.getInt("id");
            address = jMeal.getString("address");
            latitude = jMeal.getString("latitude");
            longitude = jMeal.getString("longitude");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        MealLocation location = new MealLocation(id, address, latitude, longitude);

        return location;
    }

    /**
     * Parsing the User JSON object
     */
    private User getUser(JSONObject jMeal) {

        int id = 0;
        String firstName = "";
        String lastName = "";
        String photo = "";

        try {
            id = jMeal.getInt("id");
            firstName = jMeal.getString("first_name");
            lastName = jMeal.getString("last_name");
            photo = jMeal.getJSONObject("user_details").getString("photo");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        User user = new User(id, firstName, lastName, photo);

        return user;

    }

    private LinkedList<MealSchedule> getSchedules(JSONArray jSchedules) {

        int resultsCount = jSchedules.length();

        LinkedList<MealSchedule> scheduleList = new LinkedList<MealSchedule>();

        MealSchedule mealSchedule = new MealSchedule();

        /** Taking each meal, parses and adds to list object */
        for (int i = 0; i < resultsCount; i++) {
            try {
                /** Call getPlace with place JSON object to parse the place */
                mealSchedule = getSchedule((JSONObject) jSchedules.get(i));
                scheduleList.add(mealSchedule);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return scheduleList;
    }

    private MealSchedule getSchedule(JSONObject jSchedule) {

        int id = 0;
        String date = "";
        String startTime = "";
        String endTime = "";
        String limitTime = "";
        int guests = 0;
        int availability = 0;

        try {

            id = jSchedule.getInt("id");
            date = jSchedule.getString("date");
            startTime = jSchedule.getString("startTime");
            endTime = jSchedule.getString("endTime");
            limitTime = jSchedule.getString("limitTime");
            guests = jSchedule.getInt("guests");
            availability = jSchedule.getInt("availability");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        MealSchedule mealSchedule = new MealSchedule(id, date, startTime, endTime, limitTime, guests, availability);

        return mealSchedule;
    }

    private LinkedList<MealImage> getImages(JSONArray jImages) {

        int resultsCount = jImages.length();

        LinkedList<MealImage> imageList = new LinkedList<MealImage>();

        MealImage mealImage = new MealImage();

        /** Taking each meal, parses and adds to list object */
        for (int i = 0; i < resultsCount; i++) {
            try {
                /** Call getPlace with place JSON object to parse the place */
                mealImage = getImage((JSONObject) jImages.get(i));
                imageList.add(mealImage);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return imageList;
    }

    private MealImage getImage(JSONObject jSchedule) {

        int id = 0;
        String name = "";
        String path = "";

        try {

            id = jSchedule.getInt("id");
            name = jSchedule.getString("name");
            path = jSchedule.getString("path");

        } catch (JSONException e) {
            e.printStackTrace();
        }

        MealImage mealImage = new MealImage(id, name, path);

        return mealImage;
    }
}
