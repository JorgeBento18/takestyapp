package project1.project1;

import java.io.Serializable;

/**
 * Created by Jorge on 19-08-2015.
 */
public class User implements Serializable {

    private int id;
    private String firstName;
    private String lastName;
    private String photo;

    public User() {
    }

    public User(int id, String firstName, String lastName, String photo) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.photo = photo;
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getPhoto() {
        return photo;
    }
}
