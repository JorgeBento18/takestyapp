package project1.project1;

import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * Created by Jorge on 03-08-2015.
 */
public class RequestHelper {

    public RequestHelper() {

    }

    /**
     * Função que devolve o token usado nos pedidos post
     *
     * @return Devolve o token em forma de String se o pedido for efetuado com sucesso
     */
    public String generateGetTokenRequest() {

        List<NameValuePair> params = new ArrayList<NameValuePair>(2);
        params.add(new BasicNameValuePair("app_token", "1"));

        RequestParams requestParams = new RequestParams("get",
                "http://www.takesty.com/android/get-token?",
                params);

        try {

            JSONObject jsonObject = new GetRequest().execute(requestParams)
                    .get();

            return jsonObject.getString("_token");

        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ExecutionException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return "";
    }

    /**
     * @param email
     * @param password
     * @param token
     * @return Devolve a resposta do servidor em form de JSONObject a um pedido de signin por email
     * <p/>
     * Exemplo:
     * <p/>
     * Em caso de sucesso:
     * <p/>
     * {"success":true,"id":"2","email":"example@example.com","first_name":"John","last_name":"Doe","description":"O meu nome \u00e9 John Doe, e gosto de cozinhar para v\u00e1rias pessoas, pratos diversos, tanto de peixe como de carne, e \u00e0s vezes vegetariano tamb\u00e9m","photo":"http:\/\/192.168.119.129\/takesty\/public\/assets\/img\/account\/John_Doe_2_1429874395.jpg","country":"Leiria, Portugal","birthday":"1990-08-26","verified":"1"}
     * <p/>
     * Em caso de erro
     * <p/>
     * {"success":false,"errors":{"email":["The email field is required"],"password":["The password field is required"]}}
     */
    public JSONObject generateSignInRequest(String email, String password,
                                            String token) {

        List<NameValuePair> params = new ArrayList<NameValuePair>(2);

        params.add(new BasicNameValuePair("app_token", "1"));
        params.add(new BasicNameValuePair("_token", token));
        params.add(new BasicNameValuePair("email", email));
        params.add(new BasicNameValuePair("password", password));

        RequestParams requestParams = new RequestParams("post",
                "http://www.takesty.com/android/auth/signin",
                params);

        try {

            JSONObject jsonObject = new PostRequest().execute(requestParams)
                    .get();

            System.out.println(jsonObject);

            return jsonObject;

        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ExecutionException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return null;

    }

    /**
     * @param first_name
     * @param last_name
     * @param email
     * @param email_confirm
     * @param password
     * @param password_confirm
     * @param token
     * @return Devolve a resposta do servidor em forma de JSONObject a um pedido de Signup por email
     * <p/>
     * Exemplo:
     * <p/>
     * Em caso de sucesso:
     * <p/>
     * {"success":true,"id":"2","email":"example@example.com","first_name":"John","last_name":"Doe","description":"O meu nome \u00e9 John Doe, e gosto de cozinhar para v\u00e1rias pessoas, pratos diversos, tanto de peixe como de carne, e \u00e0s vezes vegetariano tamb\u00e9m","photo":"http:\/\/192.168.119.129\/takesty\/public\/assets\/img\/account\/John_Doe_2_1429874395.jpg","country":"Leiria, Portugal","birthday":"1990-08-26","verified":"1"}
     * <p/>
     * Em caso de erro:
     * <p/>
     * {"success":false,"errors":{"first_name":["The first name is required."],"last_name":["The last name is required."],"email":["The email field is required"],"email_confirm":["The email confirmation is required."],"password":["The password field is required"],"password_confirm":["The password confirmation is required."]}}n
     */
    public JSONObject generateSignUpRequest(String first_name,
                                            String last_name, String email, String email_confirm,
                                            String password, String password_confirm, String token) {

        List<NameValuePair> params = new ArrayList<NameValuePair>(2);

        params.add(new BasicNameValuePair("app_token", "1"));
        params.add(new BasicNameValuePair("_token", token));
        params.add(new BasicNameValuePair("first_name", first_name));
        params.add(new BasicNameValuePair("last_name", last_name));
        params.add(new BasicNameValuePair("email", email));
        params.add(new BasicNameValuePair("email_confirm", email_confirm));
        params.add(new BasicNameValuePair("password", password));
        params.add(new BasicNameValuePair("password_confirm", password_confirm));

        RequestParams requestParams = new RequestParams("post",
                "http://www.takesty.com/android/auth/signup",
                params);

        try {

            JSONObject jsonObject = new PostRequest().execute(requestParams)
                    .get();

            System.out.println(jsonObject);

            return jsonObject;

        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ExecutionException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return null;
    }

    /**
     * @param name
     * @param email
     * @param message
     * @param token
     * @return Devolve a resposta do servidor em forma de JSONObject a um envio de contacto
     * @throws JSONException Exemplo:
     *                       <p/>
     *                       Em caso de sucesso:
     *                       <p/>
     *                       {"success":true}
     *                       <p
     *                       Em caso de erro:
     *                       <p/>
     *                       {"success":false,"errors":{"name":["The name is required."],"email":["The email field is required"],"description":["The description is required."]}}
     */
    public JSONObject generatePostContactUs(String name, String email,
                                            String message, String token) {

        List<NameValuePair> params = new ArrayList<NameValuePair>(2);

        params.add(new BasicNameValuePair("app_token", "1"));
        params.add(new BasicNameValuePair("_token", token));
        params.add(new BasicNameValuePair("name", name));
        params.add(new BasicNameValuePair("email", email));
        params.add(new BasicNameValuePair("description", message));

        RequestParams requestParams = new RequestParams("post",
                "http://www.takesty.com/android/contact-us",
                params);

        try {

            JSONObject jsonObject = new PostRequest().execute(requestParams)
                    .get();

            System.out.println(jsonObject);

            return jsonObject;

        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ExecutionException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return null;
    }

    public JSONObject generatePlacesRequest(String place) {

        List<NameValuePair> params = new ArrayList<NameValuePair>(2);

        params.add(new BasicNameValuePair("app_token", "1"));
        params.add(new BasicNameValuePair("input", place));
        params.add(new BasicNameValuePair("types", "geocode"));
        params.add(new BasicNameValuePair("sensor", "false"));
        params.add(new BasicNameValuePair("key",
                "AIzaSyBnbpFZwYZ2SeB6ftaCkHp4FfbCSrgedWM"));

        RequestParams requestParams = new RequestParams("get",
                "https://maps.googleapis.com/maps/api/place/autocomplete/json?",
                params);

        try {

            JSONObject jsonObject = new GetRequest().execute(requestParams)
                    .get();

            System.out.println(jsonObject);

            return jsonObject;

        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ExecutionException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return null;

    }

    public JSONObject generateSearchRequest(String location, String categoryMeat, String categoryFish, String categoryVege, String categoryDessert, String date, String timeFrom, String timeTo, String price) {

        List<NameValuePair> params = new ArrayList<NameValuePair>(2);

        params.add(new BasicNameValuePair("app_token", "1"));
        params.add(new BasicNameValuePair("location", location));

        params.add(new BasicNameValuePair("categoryMeat", categoryMeat+""));
        params.add(new BasicNameValuePair("categoryFish", categoryFish+""));
        params.add(new BasicNameValuePair("categoryVege", categoryVege+""));
        params.add(new BasicNameValuePair("categoryDessert", categoryDessert+""));

        if (!date.matches("")) {
            params.add(new BasicNameValuePair("date", date));
        }

        if (!timeFrom.matches("")) {
            params.add(new BasicNameValuePair("timeFrom", timeFrom));
        }

        if (!timeTo.matches("")) {
            params.add(new BasicNameValuePair("timeTo", timeFrom));
        }

        if (!price.matches("")) {
            params.add(new BasicNameValuePair("price", price));
        }

        RequestParams requestParams = new RequestParams("get",
                "http://192.168.119.129/takesty/public/android/search?",
                params);

        try {

            JSONObject jsonObject = new GetRequest().execute(requestParams)
                    .get();

            System.out.println(jsonObject);

            return jsonObject;

        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ExecutionException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return null;

    }

    private class GetRequest extends AsyncTask<RequestParams, Void, JSONObject> {

        @Override
        protected JSONObject doInBackground(RequestParams... params) {

            RequestParams requestParams = params[0];

            HttpClient httpclient = new DefaultHttpClient();

            String paramString = URLEncodedUtils.format(
                    requestParams.getParams(), "utf-8");

            String link = requestParams.getUrl() + paramString;

            System.out.println("LINK: " + link);

            HttpGet httpGet = new HttpGet(link);

            try {

                HttpResponse response = httpclient.execute(httpGet);

                int status = response.getStatusLine().getStatusCode();

                if (status == 200) {

                    HttpEntity entity = response.getEntity();

                    String data = EntityUtils.toString(entity);

                    JSONObject json = new JSONObject(data);

                    return json;
                }

            } catch (IOException e)

            {
                e.printStackTrace();
            } catch (JSONException e)

            {

                e.printStackTrace();
            }

            return null;
        }

        // Once Music File is downloaded
        @Override
        protected void onPostExecute(JSONObject response) {

            System.out.println("ONPOSTEXECUTE: " + response);

        }

    }

    private class PostRequest extends
            AsyncTask<RequestParams, Void, JSONObject> {

        @Override
        protected JSONObject doInBackground(RequestParams... params) {

            RequestParams requestParams = params[0];

            HttpClient httpClient = new DefaultHttpClient();

            HttpPost httpPost = new HttpPost(requestParams.getUrl());

            try {

                httpPost.setEntity(new UrlEncodedFormEntity(requestParams
                        .getParams()));

                HttpResponse response = httpClient.execute(httpPost);

                int status = response.getStatusLine().getStatusCode();

                System.out.println("STATUS: " + status);

                if (status == 200) {

                    HttpEntity entity = response.getEntity();

                    String data = EntityUtils.toString(entity);

                    JSONObject json = new JSONObject(data);

                    return json;
                } else {

                    String reasonPhrase = response.getStatusLine()
                            .getReasonPhrase();

                    System.out.println("ERROR MESSAGE: " + reasonPhrase);

                }

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (ClientProtocolException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            return null;
        }

        // Once Music File is downloaded
        @Override
        protected void onPostExecute(JSONObject response) {

            System.out.println("ONPOSTEXECUTE: " + response);

        }
    }
}
