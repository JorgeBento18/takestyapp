package project1.project1;

import java.io.Serializable;
import java.util.LinkedList;

/**
 * Created by Jorge on 19-08-2015.
 */
public class Meal implements Serializable {

    private int id;
    private String title;
    private String description;
    private String contact;
    private double price;
    private String category;
    private int aproved;
    private int shared;
    private User user;
    private MealLocation location;
    private LinkedList<MealSchedule> schedules;
    private LinkedList<MealImage> images;

    public Meal() {
    }

    public Meal(int id, String title, String description, String contact, double price, String category, int aproved, int shared, User user, MealLocation location, LinkedList<MealSchedule> schedules, LinkedList<MealImage> images) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.contact = contact;
        this.price = price;
        this.category = category;
        this.aproved = aproved;
        this.shared = shared;
        this.user = user;
        this.location = location;
        this.schedules = schedules;
        this.images = images;
    }

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getContact() {
        return contact;
    }

    public double getPrice() {
        return price;
    }

    public String getCategory() {
        return category;
    }

    public int getAproved() {
        return aproved;
    }

    public int getShared() {
        return shared;
    }

    public User getUser() {
        return user;
    }

    public MealLocation getLocation() {
        return location;
    }

    public LinkedList<MealSchedule> getSchedules() {
        return schedules;
    }

    public LinkedList<MealImage> getImages() {
        return images;
    }
}
