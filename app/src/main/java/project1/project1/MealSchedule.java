package project1.project1;

import java.io.Serializable;

/**
 * Created by Jorge on 19-08-2015.
 */
public class MealSchedule implements Serializable {

    private int id;
    private String date;
    private String startTime;
    private String endTime;
    private String limitTime;
    private int guests;
    private int availability;

    public MealSchedule() {
    }

    public MealSchedule(int id, String date, String startTime, String endTime, String limitTime, int guests, int availability) {
        this.id = id;
        this.date = date;
        this.startTime = startTime;
        this.endTime = endTime;
        this.limitTime = limitTime;
        this.availability = availability;
        this.guests = guests;
    }

    public int getId() {
        return id;
    }

    public String getDate() {
        return date;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public String getLimitTime() {
        return limitTime;
    }

    public int getGuests() {
        return guests;
    }

    public int getAvailability() {
        return availability;
    }
}
