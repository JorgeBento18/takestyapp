package project1.project1;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.gesture.Prediction;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends FragmentActivity implements ActionBar.TabListener {

    ActionBar actionbar;
    ViewPager viewpager;
    FragmentPageAdapter ft;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);

        setContentView(R.layout.activity_main);

        viewpager = (ViewPager) findViewById(R.id.pager);

        ft = new FragmentPageAdapter(getSupportFragmentManager());

        actionbar = getActionBar();

        viewpager.setAdapter(ft);

        actionbar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        actionbar.addTab(actionbar.newTab().setIcon(R.mipmap.ic_home).setTabListener(this));

        actionbar.addTab(actionbar.newTab().setText("Mensagens").setTabListener(this));

        actionbar.addTab(actionbar.newTab().setIcon(R.mipmap.ic_drawer).setTabListener(this));

        viewpager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int arg0) {
                actionbar.setSelectedNavigationItem(arg0);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
                // TODO Auto-generated method stub
            }
        });

        boolean ifOnline = isConnectingToInternet();

        if (ifOnline == false) {
            Toast.makeText(getApplicationContext(), "Está offline", Toast.LENGTH_LONG).show();

        }



       /* RequestHelper requestHelper = new RequestHelper();

        String token = requestHelper.generateGetTokenRequest();


        JSONObject response = requestHelper.generatePostContactUs("C-18", "android18@dbz.com", "Mensagem de teste", token);

        if (response != null) {

            try {
                if (response.getBoolean("success") == true) {

                    System.out.println("Mensagem enviada");

                } else {

                    System.out.println("Erros no envio");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }*/
    }


    @Override
    public void onTabReselected(Tab tab, FragmentTransaction ft) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onTabSelected(Tab tab, FragmentTransaction ft) {
        viewpager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(Tab tab, FragmentTransaction ft) {
        // TODO Auto-generated method stub
    }

    private boolean isConnectingToInternet() {
        ConnectivityManager connectivity = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }

        }
        return false;
    }
}
