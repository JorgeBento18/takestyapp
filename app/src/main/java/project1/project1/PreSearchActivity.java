package project1.project1;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.edmodo.rangebar.RangeBar;
import com.roomorama.caldroid.CaldroidFragment;
import com.roomorama.caldroid.CaldroidListener;
import com.yahoo.mobile.client.android.util.rangeseekbar.RangeSeekBar;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;


public class PreSearchActivity extends FragmentActivity implements View.OnClickListener {

    private AutoCompleteTextView locationAutoCompleteTextView;
    private CheckBox meatCheckBox;
    private CheckBox fishCheckBox;
    private CheckBox vegeCheckBox;
    private CheckBox dessertCheckBox;
    private Button dateButton;
    private CaldroidFragment dialogCaldroidFragment;
    private SimpleDateFormat formatter;
    private SimpleDateFormat timeFormatter;
    private Bundle state;
    private EditText dateEditText;
    private TextView minTextView;
    private TextView maxTextView;
    private Button startTimeButton;
    private EditText startTimeEditText;
    private Button endTimeButton;
    private EditText endTimeEditText;
    private Date startTime;
    private Date endTime;
    Calendar dateAndTime = Calendar.getInstance();
    private RangeBar rangebar;
    private Button searchButton;
    private RequestHelper requestHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pre_search);

        requestHelper = new RequestHelper();

        locationAutoCompleteTextView = (AutoCompleteTextView) findViewById(R.id.location_AutoCompleteTextView);

        locationAutoCompleteTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                JSONObject response = requestHelper.generatePlacesRequest(s
                        .toString());

                PlaceJSONParser placeJsonParser = new PlaceJSONParser();

                List<HashMap<String, String>> places = null;

                places = placeJsonParser.parse(response);

                String[] from = new String[]{"description"};
                int[] to = new int[]{android.R.id.text1};

                // Creating a SimpleAdapter for the AutoCompleteTextView
                SimpleAdapter adapter = new SimpleAdapter(getBaseContext(),
                        places, android.R.layout.simple_list_item_1, from, to);

                // Setting the adapter
                locationAutoCompleteTextView.setAdapter(adapter);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        meatCheckBox = (CheckBox) findViewById(R.id.meat_checkbox);
        fishCheckBox = (CheckBox) findViewById(R.id.fish_checkbox);
        vegeCheckBox = (CheckBox) findViewById(R.id.vege_checkbox);
        dessertCheckBox = (CheckBox) findViewById(R.id.dessert_checkbox);

        dateButton = (Button) findViewById(R.id.date_button);

        dateEditText = (EditText) findViewById(R.id.date_edittext);

        dateButton.setOnClickListener(this);

        startTimeButton = (Button) findViewById(R.id.starttime_button);

        startTimeButton.setOnClickListener(this);

        endTimeButton = (Button) findViewById(R.id.endtime_button);

        endTimeButton.setOnClickListener(this);

        startTimeEditText = (EditText) findViewById(R.id.starttime_edittext);

        endTimeEditText = (EditText) findViewById(R.id.endtime_edittext);

        searchButton = (Button) findViewById(R.id.search_button);

        searchButton.setOnClickListener(this);

        formatter = new SimpleDateFormat("yyyy/MM/dd");

        timeFormatter = new SimpleDateFormat("hh:mm");

        state = savedInstanceState;

        // Setup the new range seek bar
        RangeSeekBar<Integer> rangeSeekBar = new RangeSeekBar<Integer>(this);
        // Set the range
        rangeSeekBar.setRangeValues(3, 20);
        rangeSeekBar.setSelectedMinValue(3);
        rangeSeekBar.setSelectedMaxValue(20);

        rangeSeekBar.setOnRangeSeekBarChangeListener(new RangeSeekBar.OnRangeSeekBarChangeListener<Integer>() {
            @Override
            public void onRangeSeekBarValuesChanged(RangeSeekBar<?> rangeSeekBar, Integer integer, Integer t1) {

                minTextView.setText(rangeSeekBar.getSelectedMinValue() + "€");
                maxTextView.setText(rangeSeekBar.getSelectedMaxValue() + "€");
            }
        });

        // Add to layout
        RelativeLayout layout = (RelativeLayout) findViewById(R.id.seekRange_relativelayout);
        layout.addView(rangeSeekBar);

        rangebar = (RangeBar) findViewById(R.id.rangebar);
        rangebar.setTickCount(20);
       /*  rangebar.setTickHeight(25);
        rangebar.setBarWeight(6);*/
       // rangebar.setThumbIndices(3, 20);
        rangebar.setLeft(3);
        rangebar.setRight(20);

        rangebar.setOnRangeBarChangeListener(new RangeBar.OnRangeBarChangeListener() {
            @Override
            public void onIndexChangeListener(RangeBar rangeBar, int i, int i1) {

                System.out.println("LEFT: " + rangeBar.getLeftIndex());
                System.out.println("RIGHT: " + rangeBar.getRightIndex());

                System.out.println("I: " + i);
                System.out.println("I1: " + i1);

            }
        });

    }


    final CaldroidListener listener = new CaldroidListener() {

        @Override
        public void onSelectDate(Date date, View view) {

            dateEditText.setText(formatter.format(date));

            dialogCaldroidFragment.dismiss();
        }
    };


    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.date_button:

                dialogCaldroidFragment = new CaldroidFragment();

                dialogCaldroidFragment.setCaldroidListener(listener);

                // If activity is recovered from rotation
                final String dialogTag = "CALDROID_DIALOG_FRAGMENT";
                if (state != null) {

                    dialogCaldroidFragment.restoreDialogStatesFromKey(
                            getSupportFragmentManager(), state,
                            "DIALOG_CALDROID_SAVED_STATE", dialogTag);

                    Bundle args = dialogCaldroidFragment.getArguments();

                    if (args == null) {
                        args = new Bundle();
                        dialogCaldroidFragment.setArguments(args);
                    }

                    args.putString(CaldroidFragment.DIALOG_TITLE,
                            "Seleciona uma data");


                } else {
                    // Setup arguments
                    Bundle bundle = new Bundle();
                    // Setup dialogTitle
                    bundle.putString(CaldroidFragment.DIALOG_TITLE,
                            "Seleccione uma data");

                    dialogCaldroidFragment.setArguments(bundle);
                }

                dialogCaldroidFragment.show(getSupportFragmentManager(),
                        dialogTag);


                break;

            case R.id.starttime_button:

                TimePickerDialog.OnTimeSetListener t = new TimePickerDialog.OnTimeSetListener() {
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        if (endTimeEditText.getText().toString().matches("")) {

                            if (hourOfDay < 10) {

                                startTimeEditText.setText("+" + hourOfDay);
                            } else {
                                startTimeEditText.setText(hourOfDay + "");
                            }

                            if (minute < 10) {
                                startTimeEditText.setText(startTimeEditText.getText().toString() + ":0" + minute);
                            } else {
                                startTimeEditText.setText(startTimeEditText.getText().toString() + ":" + minute);
                            }

                            try {
                                startTime = timeFormatter.parse(hourOfDay + ":" + minute);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                        } else {

                            try {
                                startTime = timeFormatter.parse(hourOfDay + ":" + minute);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            if (startTime.getTime() < endTime.getTime()) {

                                if (hourOfDay < 10) {

                                    startTimeEditText.setText("+" + hourOfDay);
                                } else {
                                    startTimeEditText.setText(hourOfDay + "");
                                }

                                if (minute < 10) {
                                    startTimeEditText.setText(startTimeEditText.getText().toString() + ":0" + minute);
                                } else {
                                    startTimeEditText.setText(startTimeEditText.getText().toString() + ":" + minute);
                                }

                            } else {
                                Toast.makeText(getApplicationContext(), "A hora de inicio é inválida.", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                };

                TimePickerDialog startTimePicker = new TimePickerDialog(PreSearchActivity.this,
                        t,
                        dateAndTime.get(Calendar.HOUR_OF_DAY),
                        dateAndTime.get(Calendar.MINUTE),
                        true);

                startTimePicker.setTitle("Hora de Inicio");

                startTimePicker.setButton(TimePickerDialog.BUTTON_POSITIVE, "Ok", startTimePicker);

                startTimePicker.show();

                break;

            case R.id.endtime_button:

                TimePickerDialog.OnTimeSetListener t2 = new TimePickerDialog.OnTimeSetListener() {
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        if (startTimeEditText.getText().toString().matches("")) {

                            if (hourOfDay < 10) {

                                endTimeEditText.setText("+" + hourOfDay);
                            } else {
                                endTimeEditText.setText(hourOfDay + "");
                            }

                            if (minute < 10) {
                                endTimeEditText.setText(endTimeEditText.getText().toString() + ":0" + minute);
                            } else {
                                endTimeEditText.setText(endTimeEditText.getText().toString() + ":" + minute);
                            }

                            try {
                                endTime = timeFormatter.parse(hourOfDay + ":" + minute);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                        } else {

                            try {
                                endTime = timeFormatter.parse(hourOfDay + ":" + minute);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            if (startTime.getTime() < endTime.getTime()) {

                                if (hourOfDay < 10) {

                                    endTimeEditText.setText("+" + hourOfDay);
                                } else {
                                    endTimeEditText.setText(hourOfDay + "");
                                }

                                if (minute < 10) {
                                    endTimeEditText.setText(endTimeEditText.getText().toString() + ":0" + minute);
                                } else {
                                    endTimeEditText.setText(endTimeEditText.getText().toString() + ":" + minute);
                                }

                            } else {

                                Toast.makeText(getApplicationContext(), "A hora de término é inválida.", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                };

                TimePickerDialog endTimeTimePicker = new TimePickerDialog(PreSearchActivity.this,
                        t2,
                        dateAndTime.get(Calendar.HOUR_OF_DAY),
                        dateAndTime.get(Calendar.MINUTE),
                        true);

                endTimeTimePicker.setTitle("Hora de Término");

                endTimeTimePicker.setButton(TimePickerDialog.BUTTON_POSITIVE, "Ok", endTimeTimePicker);

                endTimeTimePicker.show();


                break;

            case R.id.search_button:

                if (locationAutoCompleteTextView.getText().toString().matches("")) {

                    locationAutoCompleteTextView.setError("A localização é obrigatória.");

                } else {

                    String location = locationAutoCompleteTextView.getText().toString();

                    String categoryMeat = "";
                    String categoryFish = "";
                    String categoryVege = "";
                    String categoryDessert = "";

                    if (meatCheckBox.isChecked()) {
                        categoryMeat = "1";
                    } else {
                        categoryMeat = "0";
                    }

                    if (fishCheckBox.isChecked()) {
                        categoryFish = "1";
                    } else {
                        categoryFish = "0";
                    }

                    if (vegeCheckBox.isChecked()) {
                        categoryVege = "1";
                    } else {
                        categoryVege = "0";
                    }

                    if (dessertCheckBox.isChecked()) {
                        categoryDessert = "1";
                    } else {
                        categoryDessert = "0";
                    }

                    String date = dateEditText.getText().toString();

                    String timeFrom = startTimeEditText.getText().toString();

                    String timeTo = endTimeEditText.getText().toString();

                    String price = rangebar.getLeftIndex()+";"+rangebar.getRightIndex();

                    System.out.println("PRICE: " + price);

                    Intent intent = new Intent(PreSearchActivity.this, SearchActivity.class);

                    intent.putExtra("location", location);

                    intent.putExtra("categoryMeat", categoryMeat);
                    intent.putExtra("categoryFish", categoryFish);
                    intent.putExtra("categoryVege", categoryVege);
                    intent.putExtra("categoryDessert", categoryDessert);

                    intent.putExtra("date", date);

                    intent.putExtra("timeFrom", timeFrom);

                    intent.putExtra("timeTo", timeTo);

                    intent.putExtra("price", price);

                    startActivity(intent);

                   /* for (int i = 0; i< meals.size(); i++){

                        System.out.println("id: " + meals.get(i).getId());
                        System.out.println("title: " + meals.get(i).getTitle());
                        System.out.println("description: " + meals.get(i).getDescription());

                        System.out.println("MEALLOCATION ID: "+ meals.get(i).getLocation().getId());
                        System.out.println("MEALLOCATION ADDRESS: "+ meals.get(i).getLocation().getAddress());
                        System.out.println("MEALLOCATION LATITUDE: "+ meals.get(i).getLocation().getLatitude());
                        System.out.println("MEALLOCATION LONGITUDE: "+ meals.get(i).getLocation().getLongitude());

                        System.out.println("USER ID: "+ meals.get(i).getLocation().getId());
                        System.out.println("USER FIRST_NAME: "+ meals.get(i).getUser().getFirstName());
                        System.out.println("USER LAST_NAME: "+ meals.get(i).getUser().getLastName());
                        System.out.println("USER PHOTO: "+ meals.get(i).getUser().getPhoto());

                        for(int j = 0; j < meals.get(i).getSchedules().size(); j++){

                            System.out.println("SCHEDULES ID: " + meals.get(i).getSchedules().get(j).getId());
                            System.out.println("SCHEDULES DATE: " + meals.get(i).getSchedules().get(j).getDate());
                            System.out.println("SCHEDULES STARTTIME: "+ meals.get(i).getSchedules().get(j).getStartTime());
                            System.out.println("SCHEDULES ENDTIME: "+ meals.get(i).getSchedules().get(j).getEndTime());
                            System.out.println("SCHEDULES LIMITTIME: "+ meals.get(i).getSchedules().get(j).getLimitTime());
                            System.out.println("SCHEDULES GUESTS: "+ meals.get(i).getSchedules().get(j).getGuests());
                            System.out.println("SCHEDULES AVAILABILITY: "+ meals.get(i).getSchedules().get(j).getAvailability());
                        }

                        for(int k = 0; k < meals.get(i).getImages().size(); k++){

                            System.out.println("IMAGES ID: " + meals.get(i).getImages().get(k).getId());
                            System.out.println("IMAGES NAME: " + meals.get(i).getImages().get(k).getName());
                            System.out.println("IMAGES PATH: "+ meals.get(i).getImages().get(k).getPath());

                        }
                    }*/
                }

                break;

            default:
                break;
        }
    }
}
