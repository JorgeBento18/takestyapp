package project1.project1;

import android.app.Activity;
import android.app.ProgressDialog;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.LinkedList;


public class SearchActivity extends Activity {

    private ListView searchResultListView;
    private SearchResultAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        searchResultListView = (ListView) findViewById(R.id.search_results_listview);

        ProgressDialog progress = new ProgressDialog(this);
        progress.setTitle("Loading");
        progress.setMessage("Wait while loading...");
        progress.show();

        RequestHelper requestHelper = new RequestHelper();

        String location = getIntent().getStringExtra("location");

        this.setTitle(location);

        String categoryMeat = getIntent().getStringExtra("categoryMeat");
        String categoryFish = getIntent().getStringExtra("categoryFish");
        String categoryVege = getIntent().getStringExtra("categoryVege");
        String categoryDessert = getIntent().getStringExtra("categoryDessert");

        String date = getIntent().getStringExtra("date");
        String timeFrom = getIntent().getStringExtra("timeFrom");
        String timeTo = getIntent().getStringExtra("timeTo");
        String price = getIntent().getStringExtra("price");

        JSONObject response = requestHelper.generateSearchRequest(location, categoryMeat, categoryFish, categoryVege, categoryDessert, date, timeFrom, timeTo, price);

        SearchResultJSONParser searchResultJsonParser = new SearchResultJSONParser();

        LinkedList<Meal> meals = searchResultJsonParser.parse(response);

        if (meals == null) {

            Toast.makeText(getApplicationContext(), "Não existem resultados", Toast.LENGTH_SHORT).show();

            TextView emptyElement = (TextView) findViewById(R.id.emptyElement);

            searchResultListView.setEmptyView(findViewById(R.id.emptyElement));

            progress.dismiss();

        } else {

            if (meals.size() == 0) {

                TextView emptyElement = (TextView) findViewById(R.id.emptyElement);

                searchResultListView.setEmptyView(findViewById(R.id.emptyElement));

                progress.dismiss();

                this.setTitle(location + " (0 resultados)");

            } else {

                this.setTitle(location + " ("+ meals.size() + " resultados)");

                adapter = new SearchResultAdapter(meals, SearchActivity.this);

                searchResultListView.setAdapter(adapter);

                progress.dismiss();

            }


        }


    }

}
