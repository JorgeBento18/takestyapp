package project1.project1;

import org.apache.http.NameValuePair;
import org.apache.http.

import java.util.List;

/**
 * Created by Jorge on 05-08-2015.
 */
public class RequestParams {

    private String method;
    private String url;
    private List<NameValuePair> params;

    public RequestParams(String method, String url, List<NameValuePair> params) {
        super();
        this.method = method;
        this.url = url;
        this.params = params;
    }

    public String getMethod() {
        return method;
    }

    public String getUrl() {
        return url;
    }

    public List<NameValuePair> getParams() {
        return params;
    }

}
