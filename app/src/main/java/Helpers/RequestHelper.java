package Helpers;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;


import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.SimpleAdapter;

import Models.RequestParams;


/**
 * Created by Jorge on 31-08-2015.
 */
public class RequestHelper {

    public RequestHelper() {

    }

    /**
     * Função que devolve o token usado nos pedidos post
     *
     * @return Devolve o token em forma de String se o pedido for efetuado com sucesso
     */
    public String generateGetTokenRequest() {

        HashMap<String, String> parameters = new HashMap<String, String>();

        parameters.put("app_token", "1");

        String parametersString = this.addParameters(parameters);

        try {

            JSONObject jsonObject = new GetRequest().execute(
                    "http://192.168.119.129/takesty/public/android/get-token?"
                            + parametersString).get();

            return jsonObject.getString("_token");

        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ExecutionException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return "";
    }

    /**
     * @param email
     * @param password
     * @param token
     * @return Devolve a resposta do servidor em form de JSONObject a um pedido de signin por email
     * <p/>
     * Exemplo:
     * <p/>
     * Em caso de sucesso:
     * <p/>
     * {"success":true,"id":"2","email":"example@example.com","first_name":"John","last_name":"Doe","description":"O meu nome \u00e9 John Doe, e gosto de cozinhar para v\u00e1rias pessoas, pratos diversos, tanto de peixe como de carne, e \u00e0s vezes vegetariano tamb\u00e9m","photo":"http:\/\/192.168.119.129\/takesty\/public\/assets\/img\/account\/John_Doe_2_1429874395.jpg","country":"Leiria, Portugal","birthday":"1990-08-26","verified":"1"}
     * <p/>
     * Em caso de erro
     * <p/>
     * {"success":false,"errors":{"email":["The email field is required"],"password":["The password field is required"]}}
     */
    public JSONObject generateSignInRequest(String email, String password,
                                            String token) {

        HashMap<String, String> parameters = new HashMap<String, String>();

        parameters.put("app_token", "1");
        parameters.put("_token", token);
        parameters.put("email", email);
        parameters.put("password", password);

        String parametersString = this.addParameters(parameters);

        RequestParams requestParams = new RequestParams(
                "http://192.168.119.129/takesty/public/android/auth/signin",
                parametersString);

        try {

            JSONObject jsonObject = new PostRequest().execute(requestParams)
                    .get();

            System.out.println(jsonObject);

            return jsonObject;

        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ExecutionException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return null;

    }

    /**
     * @param first_name
     * @param last_name
     * @param email
     * @param email_confirm
     * @param password
     * @param password_confirm
     * @param token
     * @return Devolve a resposta do servidor em forma de JSONObject a um pedido de Signup por email
     * <p/>
     * Exemplo:
     * <p/>
     * Em caso de sucesso:
     * <p/>
     * {"success":true,"id":"2","email":"example@example.com","first_name":"John","last_name":"Doe","description":"O meu nome \u00e9 John Doe, e gosto de cozinhar para v\u00e1rias pessoas, pratos diversos, tanto de peixe como de carne, e \u00e0s vezes vegetariano tamb\u00e9m","photo":"http:\/\/192.168.119.129\/takesty\/public\/assets\/img\/account\/John_Doe_2_1429874395.jpg","country":"Leiria, Portugal","birthday":"1990-08-26","verified":"1"}
     * <p/>
     * Em caso de erro:
     * <p/>
     * {"success":false,"errors":{"first_name":["The first name is required."],"last_name":["The last name is required."],"email":["The email field is required"],"email_confirm":["The email confirmation is required."],"password":["The password field is required"],"password_confirm":["The password confirmation is required."]}}n
     */
    public JSONObject generateSignUpRequest(String first_name,
                                            String last_name, String email, String email_confirm,
                                            String password, String password_confirm, String token) {

        HashMap<String, String> parameters = new HashMap<String, String>();

        parameters.put("app_token", "1");
        parameters.put("_token", token);
        parameters.put("first_name", first_name);
        parameters.put("last_name", last_name);
        parameters.put("email", email);
        parameters.put("email_confirm", email_confirm);
        parameters.put("password", password);
        parameters.put("password_confirm", password_confirm);

        String parametersString = this.addParameters(parameters);

        RequestParams requestParams = new RequestParams(
                "http://192.168.119.129/takesty/public/android/auth/signup",
                parametersString);

        try {

            JSONObject jsonObject = new PostRequest().execute(requestParams)
                    .get();

            System.out.println(jsonObject);

            return jsonObject;

        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ExecutionException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return null;
    }

    /**
     * @param name
     * @param email
     * @param message
     * @param token
     * @return Devolve a resposta do servidor em forma de JSONObject a um envio de contacto
     * @throws JSONException Exemplo:
     *                       <p/>
     *                       Em caso de sucesso:
     *                       <p/>
     *                       {"success":true}
     *                       <p
     *                       Em caso de erro:
     *                       <p/>
     *                       {"success":false,"errors":{"name":["The name is required."],"email":["The email field is required"],"description":["The description is required."]}}
     */
    public JSONObject generatePostContactUs(String name, String email,
                                            String message, String token) {

        HashMap<String, String> parameters = new HashMap<String, String>();

        parameters.put("app_token", "1");
        parameters.put("_token", token);
        parameters.put("name", name);
        parameters.put("email", email);
        parameters.put("description", message);

        String parametersString = this.addParameters(parameters);

        RequestParams requestParams = new RequestParams(
                "http://192.168.119.129/takesty/public/android/contact-us",
                parametersString);

        try {

            JSONObject jsonObject = new PostRequest().execute(requestParams)
                    .get();

            System.out.println(jsonObject);

            return jsonObject;

        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ExecutionException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return null;
    }

    public JSONObject generatePlacesRequest(String place) {

        HashMap<String, String> parameters = new HashMap<String, String>();

        parameters.put("input", place);
        parameters.put("types", "geocode");
        parameters.put("sensor", "false");
        parameters.put("key", "AIzaSyBnbpFZwYZ2SeB6ftaCkHp4FfbCSrgedWM");

        String parametersString = this.addParameters(parameters);

        try {

            JSONObject jsonObject = new GetRequest().execute(
                    "https://maps.googleapis.com/maps/api/place/autocomplete/json?"
                            + parametersString).get();

            System.out.println(jsonObject);

            return jsonObject;

        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ExecutionException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return null;

    }

    public JSONObject generateSearchRequest(String location, String categoryMeat, String categoryFish, String categoryVege, String categoryDessert, String date, String timeFrom, String timeTo, String price) {

        HashMap<String, String> parameters = new HashMap<String, String>();

        parameters.put("app_token", "1");
        parameters.put("location", location);
        parameters.put("categoryMeat", categoryMeat + "");
        parameters.put("categoryFish", categoryFish + "");
        parameters.put("categoryVege", categoryVege + "");
        parameters.put("categoryDessert", categoryDessert + "");

        if (!date.matches("")) {
            parameters.put("date", date);
        }

        if (!timeFrom.matches("")) {
            parameters.put("timeFrom", timeFrom);
        }

        if (!timeTo.matches("")) {
            parameters.put("timeTo", timeFrom);
        }

        if (!price.matches("")) {
            parameters.put("price", price);
        }

        String parametersString = this.addParameters(parameters);

        try {

            JSONObject jsonObject = new GetRequest().execute("http://192.168.119.129/takesty/public/android/search?" + parametersString)
                    .get();

            System.out.println(jsonObject);

            return jsonObject;

        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ExecutionException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return null;

    }

    private class GetRequest extends AsyncTask<String, Void, JSONObject> {

        @Override
        protected JSONObject doInBackground(String... url) {

            System.out.println("URL: " + url[0]);

            HttpURLConnection httpURLConnection = null;

            JSONObject json = null;

            try {

                URL u = new URL(url[0]);

                httpURLConnection = (HttpURLConnection) u.openConnection();

                httpURLConnection.setRequestMethod("GET");

                int status = httpURLConnection.getResponseCode();

                System.out.println("STATUS: " + status);

                if (status == 200) {

                    BufferedReader bufferedReader = new BufferedReader(
                            new InputStreamReader(
                                    httpURLConnection.getInputStream()));

                    StringBuilder stringBuilder = new StringBuilder();

                    String line = "";

                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuilder.append(line + '\n');
                    }

                    String jsonString = stringBuilder.toString();

                    System.out.println("JSONSTRING" + jsonString);

                    json = new JSONObject(jsonString);

                }

            } catch (MalformedURLException e) {
                e.printStackTrace();

            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } finally {
                httpURLConnection.disconnect();
            }

            return json;
        }

        // Once Music File is downloaded
        @Override
        protected void onPostExecute(JSONObject response) {

            System.out.println("ONPOSTEXECUTE: " + response);

        }

    }

    private class PostRequest extends
            AsyncTask<RequestParams, Void, JSONObject> {

        @Override
        protected JSONObject doInBackground(RequestParams... params) {

            RequestParams requestParams = params[0];

            HttpURLConnection httpURLConnection = null;

            JSONObject json = null;

            try {

                URL u = new URL(requestParams.getUrl());

                httpURLConnection = (HttpURLConnection) u.openConnection();

                httpURLConnection.setRequestMethod("POST");

                OutputStreamWriter writer = new OutputStreamWriter(
                        httpURLConnection.getOutputStream());

                writer.write(requestParams.getParams());
                writer.flush();

                int status = httpURLConnection.getResponseCode();

                System.out.println("STATUS: " + status);

                if (status == 200) {

                    BufferedReader bufferedReader = new BufferedReader(
                            new InputStreamReader(
                                    httpURLConnection.getInputStream()));

                    StringBuilder stringBuilder = new StringBuilder();

                    String line = "";

                    while ((line = bufferedReader.readLine()) != null) {
                        stringBuilder.append(line + '\n');
                    }

                    String jsonString = stringBuilder.toString();

                    System.out.println("JSONSTRING" + jsonString);

                    json = new JSONObject(jsonString);

                }

            } catch (MalformedURLException e) {
                e.printStackTrace();

            } catch (ProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } finally {
                httpURLConnection.disconnect();
            }

            return json;
        }

        // Once Music File is downloaded
        @Override
        protected void onPostExecute(JSONObject response) {

            System.out.println("ONPOSTEXECUTE: " + response);

        }
    }

    private String addParameters(HashMap<String, String> parameters) {

        StringBuilder postData = new StringBuilder();

        for (Map.Entry<String, String> param : parameters.entrySet()) {

            if (postData.length() != 0) {
                postData.append('&');
            }

            try {

                postData.append(URLEncoder.encode(param.getKey(), "UTF-8"));
                postData.append('=');
                postData.append(URLEncoder.encode(
                        String.valueOf(param.getValue()), "UTF-8"));

            } catch (UnsupportedEncodingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }

        String urlParameters = postData.toString();

        return urlParameters;
    }
}
