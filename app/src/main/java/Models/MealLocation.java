package Models;

import java.io.Serializable;

/**
 * Created by Jorge on 31-08-2015.
 */
public class MealLocation implements Serializable {

    private int id;
    private String address;
    private String latitude;
    private String longitude;

    public MealLocation() {
    }

    public MealLocation(int id, String address, String latitude, String longitude) {
        this.id = id;
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public int getId() {
        return id;
    }

    public String getAddress() {
        return address;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }
}
