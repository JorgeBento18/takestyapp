package Models;

import java.io.Serializable;

/**
 * Created by Jorge on 31-08-2015.
 */
public class MealImage implements Serializable {

    private int id;
    private String name;
    private String path;

    public MealImage() {
    }

    public MealImage(int id, String name, String path) {

        this.id = id;
        this.name = name;
        this.path = path;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPath() {
        return path;
    }
}
